USE [pruebatecnica]
GO

/****** Object:  StoredProcedure [dbo].[changeSalary]    Script Date: 17/04/2019 06:44:27 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[changeSalary]
	@employeeId int, 
	@supervisorId int,
	@newSalary decimal(10,2)
AS
BEGIN
	IF (@newSalary>0)
		BEGIN
		DECLARE @msg varchar(50) ;
			BEGIN TRY
				DECLARE @move varchar(50) = '';
				DECLARE @last_salary decimal(10,2) = (SELECT salary from employee where id = @employeeId);
				if(@last_salary>@newSalary)
					BEGIN 
						set @move = 'decremento'
					END
				ELSE If(@last_salary<@newSalary)
					BEGIN
						DECLARE @dobleSalary float = 2*@last_salary
						IF(@newSalary>=@dobleSalary) 
							BEGIN 
								set @msg = 'El nuevo sueldo no debe ser mas del doble del anterior';
								THROW 60000,@msg,1;
							END
						set @move = 'aumento'
					END
				ELSE
					BEGIN
						set @msg = 'El nuevo sueldo debe ser mayor o menor al anterior';
						THROW 60000,@msg,1;
					END
				BEGIN TRANSACTION
				  INSERT INTO [dbo].[history_salary]
							   ([move]
							   ,[employee_id]
							   ,[supervisor_id]
							   ,[last_salary]
							   ,[new_salary]
							   ,[created_by])
						 VALUES
							   (@move
							   ,@employeeId
							   ,@supervisorId
							   ,@last_salary
							   ,@newSalary
							   ,@supervisorId);
					UPDATE [dbo].[employee]
						SET 
							[salary] = @newSalary
							,[updated_at] = getDate()
							,[updated_by] = @supervisorId
						WHERE id= @employeeId;
				COMMIT TRANSACTION
			END TRY
			BEGIN CATCH
				THROW ;
			END CATCH
		END
	ELSE
		BEGIN
			SET @msg = 'El nuevo sueldo debe ser mayor a cero';
			THROW 60000,@msg,1;
		END
END
GO

