USE [pruebatecnica]
GO

/****** Object:  StoredProcedure [dbo].[login]    Script Date: 17/04/2019 06:44:47 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[login] 
	@username varchar(10), 
	@password varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		DECLARE @msg varchar(50);
		DECLARE @pass varchar(20)= (SELECT password FROM supervisor WHERE id= @username);
		IF(@password IS NULL)
			BEGIN
				SET @msg = 'Password is empty';
				THROW 50003, @msg,1;
			END
		IF(@pass IS NOT NULL)
			BEGIN
				IF(@password!=@pass)
					BEGIN
						SET @msg = 'Password wrong';
						THROW 50002, @msg,1;
					END
				ELSE
					BEGIN
						SELECT id FROM supervisor WHERE id= @username;
					END
			END
		ELSE
			BEGIN
				SET @msg = 'User not found';
				THROW 50001, @msg,1;
			END
	END TRY
	BEGIN CATCH
		THROW;
	END CATCH
END
GO

