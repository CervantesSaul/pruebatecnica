USE [pruebatecnica]
GO

/****** Object:  Table [dbo].[history_salary]    Script Date: 17/04/2019 06:46:01 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[history_salary](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[employee_id] [int] NOT NULL,
	[supervisor_id] [int] NOT NULL,
	[last_salary] [decimal](10, 2) NOT NULL,
	[new_salary] [decimal](10, 2) NOT NULL,
	[status] [int] NULL,
	[created_at] [datetime] NULL,
	[created_by] [int] NULL,
	[move] [varchar](50) NULL,
 CONSTRAINT [PK_history_salary] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[history_salary] ADD  CONSTRAINT [DF_history_salary_status]  DEFAULT ((1)) FOR [status]
GO

ALTER TABLE [dbo].[history_salary] ADD  CONSTRAINT [DF_history_salary_created_at]  DEFAULT (getdate()) FOR [created_at]
GO

ALTER TABLE [dbo].[history_salary]  WITH CHECK ADD  CONSTRAINT [FK_employee_history_salary] FOREIGN KEY([employee_id])
REFERENCES [dbo].[employee] ([id])
GO

ALTER TABLE [dbo].[history_salary] CHECK CONSTRAINT [FK_employee_history_salary]
GO

ALTER TABLE [dbo].[history_salary]  WITH CHECK ADD  CONSTRAINT [FK_supervisor_history_salary] FOREIGN KEY([supervisor_id])
REFERENCES [dbo].[supervisor] ([id])
GO

ALTER TABLE [dbo].[history_salary] CHECK CONSTRAINT [FK_supervisor_history_salary]
GO

ALTER TABLE [dbo].[history_salary]  WITH CHECK ADD CHECK  (([move]='cancelado' OR [move]='decremento' OR [move]='aumento'))
GO

