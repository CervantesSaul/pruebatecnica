USE [pruebatecnica]
GO

/****** Object:  Table [dbo].[supervisor]    Script Date: 17/04/2019 06:46:07 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[supervisor](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[first_name] [varchar](50) NOT NULL,
	[last_name] [varchar](50) NOT NULL,
	[phone] [varchar](10) NULL,
	[email] [varchar](100) NULL,
	[address] [varchar](200) NULL,
	[username] [varchar](10) NOT NULL,
	[password] [varchar](20) NOT NULL,
	[status] [int] NOT NULL,
	[created_at] [datetime] NULL,
	[created_by] [int] NULL,
	[updated_at] [datetime] NULL,
	[updated_by] [int] NULL,
 CONSTRAINT [PK_supervisor] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[supervisor] ADD  CONSTRAINT [DF_supervisor_status]  DEFAULT ((1)) FOR [status]
GO

ALTER TABLE [dbo].[supervisor] ADD  CONSTRAINT [DF_supervisor_created_at]  DEFAULT (getdate()) FOR [created_at]
GO


