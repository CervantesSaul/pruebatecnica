﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using PruebaTecnica;

//imports
using PruebaTecnica.Models;

namespace PruebaTecnica.Controllers
{
    public class EmployeeController : ApiController
    {
        pruebatecnicaEntities entities = new pruebatecnicaEntities();

        public EmployeeController(){
            entities.Configuration.ProxyCreationEnabled = false;    
        }

        public IEnumerable<employee> Get()
        {
            
            return entities.employee.ToList();
        }

        public employee Get(int id)
        {
            return entities.employee.FirstOrDefault(e => e.id == id);
        }

        [Route("api/employee/modifySalary")]
        [HttpPost]
        public HttpResponseMessage Post([FromBody] ChangeSalary changeSalary)
        {
            try
            {
                var res = entities.changeSalary(changeSalary.employee_id,changeSalary.supervisor_id,changeSalary.new_salary);
                ResponseModel resp = new ResponseModel();

                resp.status = true;
                resp.message = "Change salary Successful";
                string json = JsonConvert.SerializeObject(resp);
                var message = Request.CreateResponse(HttpStatusCode.Accepted, json);
                message.Headers.Location = new Uri(Request.RequestUri.ToString());
                return message;
            }
            catch (Exception e)
            {
                ResponseModel resp = new ResponseModel();
                resp.error = e.InnerException.Message.ToString();
                resp.status = false;
                resp.message = e.InnerException.Message.ToString();
                string json = JsonConvert.SerializeObject(resp);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, json, e.InnerException);
            }
        }
    }
}
