﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

//imports
using PruebaTecnica.Models;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace PruebaTecnica.Controllers
{
    public class SupervisorController : ApiController
    {
        private pruebatecnicaEntities db = new pruebatecnicaEntities();

        [Route("api/auth/login")]
        [HttpPost]
        public async Task<HttpResponseMessage> PostAsync([FromBody] Login login)
        {
            try
            {
                var res = db.login(login.username, login.password);
                ResponseModel resp = new ResponseModel();
                
                resp.status = true;
                resp.message = "Login Successful";
                string json = JsonConvert.SerializeObject(resp);
                var message = Request.CreateResponse(HttpStatusCode.Accepted, json);
                message.Headers.Location = new Uri(Request.RequestUri.ToString());
                return message;
            }catch(Exception e)
            {
                ResponseModel resp = new ResponseModel();
                resp.error = e.InnerException.Message.ToString();
                resp.status = false;
                resp.message = e.InnerException.Message.ToString();
                string json = JsonConvert.SerializeObject(resp);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, json, e.InnerException);
            }
        }
    }
}
