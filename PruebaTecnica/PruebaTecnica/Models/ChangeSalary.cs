﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaTecnica.Models
{
    public class ChangeSalary
    {
        public int employee_id { get; set; }
        public int supervisor_id { get; set; }
        public decimal new_salary { get; set; }
    }
}