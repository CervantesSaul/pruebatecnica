﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaTecnica.Models
{
    public class ResponseModel
    {
        public bool status { get; set; }
        public string message { get; set; }
        public string error { get; set; }
    }
}