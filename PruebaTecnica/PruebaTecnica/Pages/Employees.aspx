﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Employees.aspx.cs" Inherits="PruebaTecnica.Pages.Employees" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Empleados</title>
    <script src="https://code.jquery.com/jquery-3.4.0.js" integrity="sha256-DYZMCC8HTC+QDr5QNaIcfR7VSPtcISykd+6eSmBW5qo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body onload="obtenerEmpleados()">
    <form id="form1" runat="server">
        <div>
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Telefono</th>
                        <th>Email</th>
                        <th>Salary</th>
                        <th>Modificar</th>
                    </tr>
                </thead>
                <tbody id="cuerpo">
                </tbody>
            <tfoot>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Telefono</th>
                <th>Email</th>
                <th>Salary</th>
                <th>Modificar</th>
            </tr>
        </tfoot>
    </table>
        </div>
    </form>

    <div class="modal" id="myModal" name="myModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="titleModal" name="titleModal">Modal title</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <div class="form-group">
                      <label class="control-label">
                          Sueldo actual: 
                      </label>
                      <label class="control-label" id="lastSalary" name="lastSalary">
                          10000
                      </label>
                      <span class="help-block"></span>
                  </div>

                  <div class="form-group">
                      <label for="newSalary" class="control-label">Nuevo sueldo</label>
                      <input type="number" class="form-control" id="newSalary" name="newSalary" title="NewSalary" placeholder="Enter the new salary" min="0" />
                      <span class="help-block"></span>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-primary" onclick="changeSalary()" >Actualizar Sueldo</button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              </div>
          </div>
      </div>
    </div>

    <script type="text/javascript">

        function obtenerEmpleados() {
            $.ajax({
                url: '/api/employee',
                method: 'GET',
                data: {

                },
                success: function (response) {
                    $("#cuerpo").html("");
                    for (var i = 0; i < response.length; i++) {
                        var tr = `<tr>
                          <td>`+ response[i].id + `</td>
                          <td>`+ response[i].first_name + `</td>
                          <td>`+ response[i].last_name + `</td>
                          <td>`+ response[i].phone + `</td>
                          <td>`+ response[i].email + `</td>
                          <td>`+ response[i].salary + `</td>
                          <td> <button type="button" class="btn" id="btnOpenModal" onClick="openModal(`+response[i].id+`)">ModificarSueldo </button></td>
                        </tr>`;
                        $("#cuerpo").append(tr)
                    }
                },
                error: function (jqXHR) {
                    alert("No se ha podido cargar los empleados");
                }
            });
        }

        function openModal(id) {
            $.ajax({
                url: '/api/employee/'+id,
                method: 'GET',
                data: {

                },
                success: function (response) {
                    $('#myModal').modal('show');
                    document.getElementById('titleModal').innerHTML = response.first_name + ' ' + response.last_name;
                    document.getElementById('lastSalary').innerHTML = response.salary;
                    localStorage.setItem('employeeId', response.id);
                },
                error: function (jqXHR) {
                    alert("No se ha podido cargar los empleados");
                }
            });
        }

        function changeSalary() {
            alert(localStorage.getItem("supervisorId"));
            var employee_id = localStorage.getItem("employeeId");
            var supervisor_id = localStorage.getItem("supervisorId");
            var new_salary = $('#newSalary').val();
            $.ajax({
                url: '/api/employee/modifySalary',
                method: 'POST',
                data: {
                    employee_id: employee_id,
                    supervisor_id: supervisor_id,
                    new_salary: new_salary
                },
                success: function (response) {
                    var res = JSON.parse(response);
                    if (res.status) {
                        alert(res.message);
                        location.reload();
                    } else {
                        alert(res.status)
                    }
                },
                error: function (jqXHR) {
                    var res = JSON.parse(jqXHR.responseText);
                    var result = JSON.parse(res.Message);
                    alert(result.message);
                }
            });
        }
    </script>

</body>
</html>
