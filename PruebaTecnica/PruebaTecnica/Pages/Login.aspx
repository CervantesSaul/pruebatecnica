﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="PruebaTecnica.Pages.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Login</title>
    <script src="https://code.jquery.com/jquery-3.4.0.js" integrity="sha256-DYZMCC8HTC+QDr5QNaIcfR7VSPtcISykd+6eSmBW5qo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body class="text-center">
    <form id="form1" runat="server">
        <div>
            <div style="position:center;" >
                <div class="modal-body" style="margin:100px 0px 0px 400px;">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="username" class="control-label">
                                    User
                                </label>
                                <input type="text" class="form-control" id="userId" name="userId" title="userId" placeholder="Enter your user id"/>
                                <span class="help-block"></span>
                            </div>

                            <div class="form-group">
                                <label for="password" class="control-label">Password</label>
                                <input type="password" class="form-control" id="pass" name="pass" title="Password" placeholder="Enter your password"/>
                                <span class="help-block"></span>
                            </div>
                            
                            <button type="button" class="btn btn-success btn-block" id="btnLogin">Sign In</button>
                            <p class="mt-5 mb-3 text-muted">&copy; 2019</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script type="text/javascript">
        $('#btnLogin').click(function () {
            localStorage.setItem('supervisorId', $('#userId').val());
            $.ajax({
                url: '/api/auth/login',
                method: 'POST',
                data: {
                    username: $('#userId').val(),
                    password: $('#pass').val()
                },
                success: function (response) {
                    var res = JSON.parse(response);
                    if (res.status) {
                        alert(res.message);
                        window.location = "Employees.aspx";
                    } else {
                        alert(res.status)
                    }
                },
                error: function (jqXHR) {
                    var res = JSON.parse(jqXHR.responseText);
                    var result = JSON.parse(res.Message);
                    alert(result.message);
                }
            });
        });
    </script>

</body>
</html>
